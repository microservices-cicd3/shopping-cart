let express = require('express');
let app = express();


app.get('/', function (req, res) {
    res.json({ 
        shoppingCart: [{
            product: "laptop", 
            number: 1,
            price: 500
        }, {
            product: "monitor",
            number: 4,
            price: 159
        }]
    });
    res.end();
});

app.listen(3002, function () {
  console.log("Shopping-cart listening on port 3002!");
});

